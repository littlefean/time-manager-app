# -*- encoding: utf-8 -*-
"""
PyCharm pyDBSet
2022年09月03日
by littlefean
"""
from typing import *


class PyDBSet:
    def __init__(self, fileName: str):
        """
        初始化一个Set类型py文件数据库
        这个结构是 Set[str]
        {"", "", }

        """
        self.file = f"myDB/pyDB/{fileName}.py"
        try:
            with open(self.file, "r", encoding="utf-8") as f:
                content = f.read()
                self.contentSet: Set[str] = set() if content.strip() == "" else eval(content)
        except FileNotFoundError:
            with open(self.file, "w", encoding="utf-8") as f:
                f.write("set()")
                self.contentSet: Set[str] = set()

    def add(self, item: str):
        self.contentSet.add(item)

    def save(self):
        with open(self.file, "w", encoding="utf-8") as f:
            f.write(repr(self.contentSet))

    def __contains__(self, item: str):
        return item in self.contentSet


def main():
    return None


if __name__ == "__main__":
    main()
