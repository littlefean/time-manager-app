# -*- encoding: utf-8 -*-
"""
PyCharm test
2022年07月05日
by littlefean
"""
from typing import *
from datetime import datetime
from datetime import timedelta


def test1():
    d = datetime.now()
    print(d.strftime("%A"))  # 字符串的星期六中文
    print(d.isoweekday())  # 1 2 3 4 5 6 7
    print()
    return None


def test2():
    s1 = "abcdefg"
    import hashlib
    m = hashlib.sha256()
    m.update(s1.encode("utf-8"))
    print(m.hexdigest())
    ...


def main():
    test1()
    ...


if __name__ == "__main__":
    main()
