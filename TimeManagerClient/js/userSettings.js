/**
 * 此文件负责用户更改主题的功能
 * by littlefean
 */


/**
 * 用户的自定义设置类
 *
 * 目前的自定义设置只有主题颜色
 *
 */
class UserSettings {
    constructor() {

        // 四种更改主题的按钮
        this.s1EleBtn = $(".choiceStyle .s1");
        this.s2EleBtn = $(".choiceStyle .s2");
        this.s3EleBtn = $(".choiceStyle .s3");
        this.s4EleBtn = $(".choiceStyle .s4");

        // 数组中元素是html标签，要被设置为比较浅色背景的标签
        this.backgroundEleList = [];
        // 数组中元素是html标签，要被设置为比较深色背景的标签
        this.pannelEleList = [];

        this.set("pinkStyle");
        this._bind();
    }

    /**
     * 获取到所有需要更新颜色的element，并添加到自身属性的数组
     * @private
     */
    _getEle() {
        // 先清空数组
        this.backgroundEleList = [];
        this.pannelEleList = [];

        // 更新背景
        for (let ele of $$(".view")) {
            this.backgroundEleList.push(ele);
        }
        for (let ele of $$(".num")) {
            this.backgroundEleList.push(ele);
        }
        // 面板
        this.pannelEleList.push($(".leftPageBtn"));
        this.pannelEleList.push($(".rightPageBtn"));
        this.pannelEleList.push($(".setModeBtn"));
        this.pannelEleList.push($(".modeChangeBtn"));
        this.pannelEleList.push($(".inputBox"));
        for (let ele of $$(".item")) {
            this.pannelEleList.push(ele);
        }
        for (let ele of $$(".checkItem")) {
            this.pannelEleList.push(ele);
        }
        for (let ele of $$(".lineBtn")) {
            this.pannelEleList.push(ele);
        }
    }

    /**
     * 更改设置
     * 此方法会更改界面的css颜色
     * 同时会更改全局属性user的style属性
     * @param styleName 字符串
     */
    set(styleName) {
        USER_DATA.style = styleName;
        this._getEle();

        /**
         *
         * div {
            background-color: ${Style[styleName].background};
            color: ${Style[styleName].normalWord};
           }
         *
         * 用js控制css，从而控制样式。
         * @type {string}
         */
        $(".jsControlCss").innerText = `
        
        .item {
            background-color: ${STYLE[styleName].panelBackground};
            color: ${STYLE[styleName].normalWord};
        }
        * {
            color: ${STYLE[styleName].normalWord};
        }
        button {
            background-color: ${STYLE[styleName].panelBackground};
            color: ${STYLE[styleName].normalWord};
        }
        .CheckTwoPanelSelected {
            border-top: 4px solid ${STYLE[styleName].deepBorder};
            border-left: 4px solid ${STYLE[styleName].deepBorder};
            border-right: 4px solid ${STYLE[styleName].deepBorder};
            border-bottom: none;
            background-color: ${STYLE[styleName].panelBackground};
            color: ${STYLE[styleName].normalWord};
        }
        `;
        for (let ele of this.backgroundEleList) {
            ele.style.backgroundColor = STYLE[styleName].background;
        }

        for (let ele of this.pannelEleList) {
            ele.style.backgroundColor = STYLE[styleName].panelBackground;
        }
    }

    /**
     * 通过网络请求的方式更新用户的颜色风格
     * 用于一开始登录加载
     */
    update() {
        AJAX("userGetSettings", {
            userName: USER_DATA.name
        }).finish(obj => this.set(obj["style"]));
    }

    /**
     * 初始化界面绑定按钮点击属性的方法
     * 此方法只在这个类的构造函数中调用
     * @private
     */
    _bind() {
        // 告诉后端更改用户颜色设置
        let sendSet = str => {
            AJAX("userSetSettings", {
                userName: USER_DATA.name,
                settings: {
                    style: str
                }
            }).finish(res => {
                if (res === "ok") {
                    Bubble.pop("设置成功");
                }
            })
        }
        this.s1EleBtn.onclick = () => {
            this.set("pinkStyle");
            sendSet("pinkStyle");
        }
        this.s2EleBtn.onclick = () => {
            this.set("blueStyle");
            sendSet("blueStyle");
        }
        this.s3EleBtn.onclick = () => {
            this.set("yellowStyle");
            sendSet("yellowStyle");
        }
        this.s4EleBtn.onclick = () => {
            this.set("blackStyle");
            sendSet("blackStyle");
        }
    }


}
