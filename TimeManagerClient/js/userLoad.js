/**
 * 从后端获取用户的所有时间种类并更新
 * 前端和全局数组
 */
function UPDATE_USER_TIME_KIND() {
    // 时间类型删除管理
    let timeKind = $('.alertChangeMode .currentPanel');
    // 加载所有时间种类
    AJAX("userGetAllTimeKind", {userName: USER_DATA.name}).finish(resArr => {
        timeKind.innerHTML = "";
        USER_DATA.addTimeKindList = resArr;
        for (let item of resArr) {
            let obj = TimeKind.FromObj(item);
            // 显示到html上
            timeKind.appendChild(obj.toHtmlEle());
        }
    });
}

/**
 * 整个界面加载用户的信息
 * 一般是当用户登录了之后触发、而不是一打开应用就触发
 *
 * 还要提前清除掉以前的画面内容
 * by littlefean
 */
function loadUser() {
    let DDLArr = [];
    let WeekEventArr = [];  // 缓存起来
    let weekEventItems = $(`.weekEventItems`);
    let DDLItems = $(`.DDLItems`);

    // 获取用户所有的倒数日
    AJAX("userGetAllDDL", {userName: USER_DATA.name}).finish(arr => {
        if (DDLArr.length === 0) {
            DDLItems.innerHTML = "";
            // 说明还没有缓存，也没有添加html
            for (let item of arr) {
                let ddl = DDL.FromObj(item);
                // 渲染到html上面
                DDLArr.push(item);
                DDLItems.appendChild(ddl.toHtmlElement());
            }
        }
    });

    // 获取用户所有的周常事件
    AJAX("userGetAllWeekEvent", {userName: USER_DATA.name}).finish(arr => {
        if (WeekEventArr.length === 0) {
            weekEventItems.innerHTML = "";
            // 说明还没有缓存，也没有添加html
            for (let item of arr) {
                let we = WeekEvent.FromObj(item);
                // 渲染到html上面
                WeekEventArr.push(item);
                weekEventItems.appendChild(we.toHtmlElement());
            }
        }
    });

    // 获取所有反思
    let page3Ele = $(`.v3`);
    // 一上来就加载
    AJAX("userGetAllReflect", {userName: USER_DATA.name}).finish(resArr => {
        page3Ele.querySelector(`.checkItemArea1`).innerHTML = "";
        for (let item of resArr) {
            let obj = MassagePiece.FromObj(item);
            // 显示到html上
            page3Ele.querySelector(`.checkItemArea1`).appendChild(obj.toHtmlEle(1));
        }
    });
    // 加载所有灵感
    AJAX("userGetAllInspiration", {userName: USER_DATA.name}).finish(resArr => {
        page3Ele.querySelector(`.checkItemArea2`).innerHTML = "";
        for (let item of resArr) {
            let obj = MassagePiece.FromObj(item);
            // 显示到html上
            page3Ele.querySelector(`.checkItemArea2`).appendChild(obj.toHtmlEle(2));
        }
    });

    // 姓名处理
    let menuBtn4 = $(`.menuBtn4`);
    menuBtn4.addEventListener("click", () => {
        $(`#userName`).innerHTML = USER_DATA.name;
    });
    UPDATE_USER_TIME_KIND();
}
