// 中间的高度是减去10% 再减去topArea高度
let heightRes = WIN_HEIGHT * 0.9 - $(`.v1TopArea`).clientHeight;
let hourHeight = (heightRes - MARGIN_T) / HOUR_LIST.length;

// 设置canvas大小
let canvasTable = new CanvasTimeTable(
    $(`.tableBgCanvas`),
    WIN_WIDTH * 0.9,
    heightRes - MARGIN_T,
    hourHeight
);

// 浏览器窗口大小发生变化事件
window.addEventListener("resize", () => {
    updateWindowSize();
    let heightRes = WIN_HEIGHT * 0.9 - $(`.v1TopArea`).clientHeight;
    let hourHeight = (heightRes - MARGIN_T) / HOUR_LIST.length;
    canvasTable.resize(WIN_WIDTH * 0.9, heightRes - MARGIN_T, hourHeight);
    rulerResize();
});

function rulerResize() {
    let heightRes = WIN_HEIGHT * 0.9 - $(`.v1TopArea`).clientHeight;
    let hourHeight = (heightRes - MARGIN_T) / HOUR_LIST.length;
    // 左侧ruler也要跟着变
    $(`.v1 .page`).style.height = heightRes + "px";
    // 设置左侧刻度尺
    let ruler = $(".ruler");
    ruler.style.height = heightRes - MARGIN_T + "px";
    ruler.style.marginTop = MARGIN_T + "px";
    ruler.innerHTML = "";
    for (let i of HOUR_LIST) {
        let b = div("", "block");
        b.style.height = hourHeight + "px";
        b.appendChild(div(`${i}`, "num"));
        ruler.appendChild(b);
    }
}

function pageInitMain() {
    // 设置页面位置
    let downMenu = document.querySelector(`.downMenu`);
    let topView = document.querySelector(`.topView`);

    for (let i = 0; i < 4; i++) {
        let menu = downMenu.children[i];
        menu.addEventListener("click", () => {
            topView.style.left = `-${100 * i}%`;
            if (i === 0) {
                canvasTable.getPeriod();
            }
        });
    }
    topView.style.left = "0";  // 一开始就将第一页设置为首页
    setTimeout(() => {  // 强制解决一打开没有显示时间段的情况
        canvasTable.getPeriod();
    }, 100);
}

function pageInit1() {

    // v1界面设置
    let v1 = document.querySelector(`.v1`);

    v1.querySelector(`.page`).style.height = heightRes + "px";

    rulerResize();

    // 设置右侧时间表
    let table = v1.querySelector(`.table`);
    let tableTitleLine = table.querySelector(`.marginT`);
    for (let i = 0; i < 7; i++) {
        // 添加标题
        let titleBlock = div("", "titleBlock");
        titleBlock.appendChild(div(WEEK_NAME_DIC[i], "weekTitle"));
        titleBlock.appendChild(div("?.??", "dateTitle"));
        tableTitleLine.appendChild(titleBlock);
    }
    /// 从观看模式切换到添加模式的btn
    let modeChangeBtn = document.querySelector(`.modeChangeBtn`);
    modeChangeBtn.onclick = function () {
        console.log("点击了切换模式");
        canvasTable.refresh();
        if (canvasTable.mood.isAdding()) {
            this.innerHTML = "正在观看模式👀";
            canvasTable.mood.setWatching();
        } else if (canvasTable.mood.isWatching()) {
            this.innerHTML = "正在添加时段✍"
            canvasTable.mood.setAdding();
        }
    }

    // 更新日期周
    function updateWeekTitle() {
        let dateTitleList = document.getElementsByClassName(`dateTitle`);
        let i = 0
        for (let dateTitle of dateTitleList) {
            dateTitle.innerText = MyDate.FromString(NOW_WEEK[i]).toStringDote();
            i++;
        }
    }

    // 写当前这一周日期的标题
    AJAX("askWeek").finish(res => {
        NOW_WEEK = res;
        updateWeekTitle();
    })


    /**
     * 点击上一周下一周，会更新全局变量周数组，
     * 并且根据全局变量更新画板
     */

    let leftPageBtn = document.querySelector(`.leftPageBtn`);
    let rightPageBtn = document.querySelector(`.rightPageBtn`);

    let f = (str, that) => {
        that.setAttribute("disabled", "disabled");
        AJAX(str, NOW_WEEK).finish(res => {
            that.removeAttribute("disabled");
            NOW_WEEK = res;
            updateWeekTitle();
            canvasTable.refresh();
        })
    }

    // 上一周
    leftPageBtn.onclick = function () {
        f("queryLastWeek", this);
    }

    // 下一周
    rightPageBtn.onclick = function () {
        f("queryNextWeek", this);
    }

    // 设置活动分类界面 ===> 添加一种时间种类
    let setModeBtn = document.querySelector(".setModeBtn");
    let timePanel = document.querySelector(`.alertChangeMode`);

    timePanel.querySelector(".close").onclick = () => {
        closeDiv(timePanel);
    };
    let cp = $(".alertChangeMode .currentPanel");
    timePanel.querySelector(".addATimeKind").onclick = () => {
        showDiv(timePanel.querySelector(".addView"))
        cp.style.display = "none"
        closeDiv(timePanel.querySelector(".addATimeKind"));
        closeDiv(timePanel.querySelector(".h2-1"));
    }
    timePanel.querySelector(".timeModeCancel").onclick = () => {
        closeDiv(timePanel.querySelector(".addView"));
        cp.style.display = "flex"
        showDiv(timePanel.querySelector(".addATimeKind"));
        showDiv(timePanel.querySelector(".h2-1"));
    };
    setModeBtn.onclick = function () {
        showDiv(timePanel);
        closeDiv(timePanel.querySelector(".addView"));
        cp.style.display = "flex"
        showDiv(timePanel.querySelector(".addATimeKind"));
        showDiv(timePanel.querySelector(".h2-1"));
    }

    // 确认添加按钮
    let timeModeAdd = timePanel.querySelector(".timeModeAdd");
    timeModeAdd.onclick = function () {
        // 先拿到三个数值属性
        let title = document.querySelector(`.KindTitle`).value;
        let fillColor = document.querySelector(`.fillColor`).value;
        let borderColor = document.querySelector(`.borderColor`).value;
        // 要发送给后端的json和给前端全局数组添加js对象
        let timeKindObj = {
            title: title,
            fillColor: fillColor,
            borderColor: borderColor,
        };
        AJAX("userAddTimeKind", {
            userName: USER_DATA.name,
            timeKind: timeKindObj
        }).finish(res => {
            if (res === "ok") {
                Bubble.pop("添加成功");

                // 更新前端显示
                USER_DATA.addTimeKindList.push(timeKindObj);
                UPDATE_USER_TIME_KIND();
                // 前端关闭
                closeDiv(timePanel);
            }
        })
    }
}

function pageInit2() {
    /**
     * 周常，倒数日界面
     */
        // 建立切换逻辑
    let page2Ele = $(`.v2`);
    let b1 = page2Ele.querySelector(`.checkItem1`);
    let b2 = page2Ele.querySelector(`.checkItem2`);
    let v1 = $(`.checkItemArea1`);
    let v2 = $(`.checkItemArea2`);
    new CheckTwoPanel(b1, b2, v1, v2);

    // 添加倒数日的按钮
    let addDDLBtn = $(`.addDDLBtn`);
    // 添加倒数日的弹窗
    let addDDLAlert = $(`.addDDLAlert`);
    let addCancel = addDDLAlert.querySelector(`.addCancel`);
    let addBtnOk = addDDLAlert.querySelector(`.addBtnOk`);

    let DDLAlertElement = new AlertEle(addDDLBtn, addBtnOk, addDDLAlert);
    DDLAlertElement.setCloseBtn(addCancel);

    // 创建一个倒数日按钮点击事件
    AlertEle.bindEvent(addBtnOk, () => {
        AJAX("userAddDDL", {
            userName: USER_DATA.name,
            ddl: {
                "title": DDLAlertElement.query(".title").value,
                "date": DDLAlertElement.query(".dateInput").value,
            }
        }).finish(num => {
            Bubble.pop("添加成功！");
            // 显示到html上，添加到缓存中
            let ddl = new DDL(
                DDLAlertElement.query(".title").value,
                DDLAlertElement.query(".dateInput").value,
            );
            ddl.remain = num;
            $(`.DDLItems`).appendChild(ddl.toHtmlElement());
        })
    });
    // 周常事件部分
    let addWeekEventBtn = $(`.addWeekEventBtn`);
    let addWeekEventAlert = $(`.addWeekEventAlert`);
    let addWEBtnOk = $(`.addWeekEventAlert .addBtnOk`);
    let weAlertElement = new AlertEle(addWeekEventBtn, addWEBtnOk, addWeekEventAlert);
    weAlertElement.setCloseBtn($(`.addWeekEventAlert .addCancel`));
    // 创建一个周常事件
    AlertEle.bindEvent(addWEBtnOk, () => {
        let we = WeekEvent.FromHtmlInput();
        AJAX("userAddWeekEvent", {
            userName: USER_DATA.name,
            weekEvent: we.toObj()
        }).finish(res => {
            if (res === "ok") {
                Bubble.pop("添加成功！");
                // 显示到html上
                $(`.weekEventItems`).appendChild(we.toHtmlElement());
            }
        })
    });

}

function pageInit3() {
    // 建立切换逻辑
    let page3Ele = $(`.v3`);
    let b1 = page3Ele.querySelector(`.checkItem1`);
    let b2 = page3Ele.querySelector(`.checkItem2`);
    let v1 = page3Ele.querySelector(`.checkItemArea1`);
    let v2 = page3Ele.querySelector(`.checkItemArea2`);
    let cp = new CheckTwoPanel(b1, b2, v1, v2);

    let inputBox = page3Ele.querySelector(".inputBox");
    let clearBtn = page3Ele.querySelector(`.clear`);

    clearBtn.addEventListener("click", () => {
        inputBox.innerHTML = "";
    });

    // 提交按钮
    let submitBtn = page3Ele.querySelector(".submit");
    submitBtn.addEventListener("click", () => {
        let u;
        if (cp._state === 1) {
            u = "userAddReflect";
        } else {
            u = "userAddInspiration";
        }
        let mp = MassagePiece.FromString(inputBox.innerHTML);
        AJAX(u, {
            userName: USER_DATA.name,
            Reflect: mp.toSendObj(),
            Inspiration: mp.toSendObj(),
        }).finish(res => {
            if (res === "ok") {
                Bubble.pop("添加成功！");
                // 显示到html上
                page3Ele.querySelector(`.checkItemArea${cp._state}`).appendChild(mp.toHtmlEle(cp._state));
            }
        })
    });
}

function pageInit4() {
    // 切换主题颜色
    let pink = $(".choiceStyle .s1");
    let blue = $(".choiceStyle .s2");
    let yellow = $(".choiceStyle .s3");
    let black = $(".choiceStyle .s4");
    pink.style.backgroundColor = STYLE.pinkStyle.background;
    pink.style.color = STYLE.pinkStyle.panelBackground;
    blue.style.backgroundColor = STYLE.blueStyle.background;
    blue.style.color = STYLE.blueStyle.panelBackground;
    yellow.style.backgroundColor = STYLE.yellowStyle.background;
    yellow.style.color = STYLE.yellowStyle.panelBackground;
    black.style.backgroundColor = STYLE.blackStyle.background;
    black.style.color = STYLE.blackStyle.panelBackground;
    // 背景颜色设置
    new UserSettings();

    // 切换账号的逻辑
    let changeCount = $(`.changeAccount`);
    changeCount.addEventListener("click", () => {
        $(".loginView").style.display = "block";
    });

}

window.onload = function () {
    pageInitMain();
    pageInit1();
    pageInit2();
    pageInit3();
    pageInit4();
    console.log(navigator.userAgent);
}
