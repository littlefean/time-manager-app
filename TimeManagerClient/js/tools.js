/**
 *
 * by littlefean
 */
function div(content = "", className = "") {
    let res = document.createElement("div");
    res.innerHTML = content;
    if (className !== "") {
        res.classList.add(className);
    }
    return res
}

function closeDiv(div) {
    if (div.style.display === "flex") {
        div.isFlex = true;
    }
    div.style.display = "none";
}

function showDiv(div) {
    if (div.isFlex === true) {
        div.style.display = "flex";
    } else {
        div.style.display = "block";
    }
}

/**
 * 让一个div展示一下一个一次性动画
 * @param div
 * @param ani
 * @param dur
 */
function alertDiv(div, ani, dur = 1) {
    if (div.style.animationName === ani) {
        return;
    }
    div.style.animationName = ani;
    div.style.animationDuration = `${dur}s`;
    div.style.animationFillMode = "forwards";
    setTimeout(() => {
        div.style.animationName = "none";
    }, 1000 * dur);
}

function $(queryStr) {
    return document.querySelector(queryStr);
}

function $$(queryStr) {
    return document.querySelectorAll(queryStr);
}


/**
 * canvas绘制标准化
 * @param n
 * @return {number}
 */
function standardize(n) {
    return Math.floor(n * PR);
}

/**
 * 返回当前设备是否是电脑
 * 如果是电脑则说名滑动添加时间段部分的功能需要用鼠标来实现。
 * @return {boolean}
 */
function isPc() {
    let info = navigator.userAgent;
    return info.includes("Windows");
}

/**
 * 手动对ajax进行二次封装
 * @param urlFix 请求路径后缀
 * @param sendJson 发送的json，如果不写则自动变成get请求
 * @return {XMLHttpRequest} 返回一个ajax对象，需要这样写
 * AJAX("userGetAllDDL", {userName: "张全蛋"}).finish((res) => {
           res 便是自动解析成结果的js对象，可以直接使用了
        });
 */
function AJAX(urlFix, sendJson = null) {
    let ajaxObj = new XMLHttpRequest();
    ajaxObj.open(sendJson === null ? "GET" : "POST", `http://${ADDRESS}:${PORT}/${urlFix}`);
    ajaxObj.setRequestHeader("Content-Type", "application/json");
    if (sendJson === null) {
        ajaxObj.send();
    } else {
        ajaxObj.send(JSON.stringify(sendJson));
    }
    ajaxObj.finish = (yourFunc) => {
        ajaxObj.onload = () => {
            let obj = JSON.parse(ajaxObj.responseText);
            yourFunc(obj);
        }
    };
    return ajaxObj;
}

/**
 * 在canvas上下文对象中画一条线，
 * 这条线是对齐了像素点的，之所以要对齐像素点就是要解决模糊的效果
 * 也就是传入的参数会默认取整
 * @param ctx
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @param lineWidth
 * @param color
 * @param isDash
 */
function drawLine(ctx, x1, y1, x2, y2,
                  lineWidth = 1, color = "black", isDash = false) {
    ctx.lineWidth = lineWidth;
    ctx.strokeStyle = color;
    ctx.beginPath();
    if (isDash) {
        ctx.setLineDash([5, 5]);
    } else {
        ctx.setLineDash([]);
    }
    ctx.moveTo(standardize(x1), standardize(y1));
    ctx.lineTo(standardize(x2), standardize(y2));
    ctx.stroke();
}

/**
 * 画矩形边框
 * @param ctx 上下文对象
 * @param x 左上角顶点
 * @param y 左上角顶点
 * @param width 宽度
 * @param height 高度
 * @param color
 * @param lineWidth
 */
function drawRectStroke(ctx, x, y, width, height, color, lineWidth) {
    ctx.strokeStyle = color;
    ctx.lineWidth = standardize(lineWidth);
    ctx.lineJoin = "round";
    ctx.strokeRect(standardize(x), standardize(y), standardize(width), standardize(height));
}

/**
 * 画矩形填充
 * @param ctx
 * @param x 左上角顶点
 * @param y 左上角顶点
 * @param width
 * @param height
 * @param color
 */
function drawRectFill(ctx, x, y, width, height, color) {
    ctx.fillStyle = color;
    ctx.fillRect(standardize(x), standardize(y), standardize(width), standardize(height));
}

/**
 * 写文字
 * @param ctx
 * @param content {String}
 * @param x {Number} px
 * @param y {Number} px
 * @param color 颜色字符串
 * @param fontSize {Number} px
 */
function writeFont(ctx, content, x, y, color = "black", fontSize = 20) {
    ctx.fillStyle = color;
    ctx.font = `${standardize(fontSize)}px "微软雅黑"`;           //设置字体
    // 默认中心对齐

    /**
     *   |
     * --+-----
     *   |
     *   也就是填入的坐标点是整个文字的中心坐标点
     */

    ctx.textBaseline = "middle";
    ctx.textAlign = "center";
    ctx.fillText(content, standardize(x), standardize(y));

}















