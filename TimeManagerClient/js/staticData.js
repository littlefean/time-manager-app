/**
 *
 * by littlefean
 */
let PORT = 10007;
// let ADDRESS = "localhost";
let ADDRESS = "124.221.150.160";

// 获取设备像素比、canvas解决模糊问题用的

let PR = window.devicePixelRatio;

// 获取窗口宽度
let WIN_WIDTH;
let WIN_HEIGHT;
/// 第一个界面中一个小条的高度
let MARGIN_T = 30;


let WEEK_NAME_DIC = ["周一", "周二", "周三", "周四", "周五", "周六", "周日"];
let HOUR_LIST = [6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1];


let NOW_WEEK = ["?", "?", "?", "?", "?", "?", "?"];

function updateWindowSize() {
    if (window.innerWidth) {
        WIN_WIDTH = window.innerWidth;
    } else if ((document.body) && (document.body.clientWidth)) {
        WIN_WIDTH = document.body.clientWidth;
    }
    // 获取窗口高度
    if (window.innerHeight) {
        WIN_HEIGHT = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)) {
        WIN_HEIGHT = document.body.clientHeight;
    }
}

updateWindowSize();

let STYLE = {
    pinkStyle: {
        background: "#fad9eb",
        panelBackground: "#ec8ec1",
        deepBorder: "#924576",
        lightWord: "#fff",
        normalWord: "#222",
        dashLine: "#333",
    },
    blueStyle: {
        background: "#d9ebfa",
        panelBackground: "#8ec6ec",
        deepBorder: "#436277",
        lightWord: "#fff",
        normalWord: "#222",
        dashLine: "#333",
    },
    yellowStyle: {
        background: "#faf0d9",
        panelBackground: "#ecd38e",
        deepBorder: "#aa9760",
        lightWord: "#fff",
        normalWord: "#222",
        dashLine: "#333",
    },
    blackStyle: {
        background: "#202020",
        panelBackground: "#414141",
        deepBorder: "#555555",
        lightWord: "#ffffff",
        normalWord: "#fff",
        dashLine: "#777",
    }
};
// 用户信息
let USER_DATA = {
    name: "张全蛋",
    password: "123456",
    style: "pinkStyle",

    /**
     * 用来存放用户的
     * @type {[TimePeriod]}
     */
    periodList: [],

    /**
     * 前端删除一个元素的方法，该方法会修改 periodList
     */
    delPeriod: function (obj) {
        let newList = [];
        for (let item of this.periodList) {
            if (JSON.stringify(item) === JSON.stringify(obj)) {
                continue;
            }
            newList.push(item);
        }
        this.periodList = newList;
    },

    // 添加时间面板的种类
    addTimeKindList: []
}

// 在最早检测用户的缓存中是否有上一次登录的用户名
USER_DATA.name = localStorage.getItem("userName");
if (localStorage.getItem("userName")) {
    USER_DATA.name = localStorage.getItem("userName");
}
